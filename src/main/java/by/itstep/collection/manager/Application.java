package by.itstep.collection.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import util.EntityManagerUtils;

import javax.swing.text.html.parser.Entity;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);

		EntityManagerUtils.getEntityManager();
	}

}
