package by.itstep.collection.manager.entity;

import by.itstep.collection.manager.entity.Collection;
import by.itstep.collection.manager.entity.Comment;
import by.itstep.collection.manager.entity.Role;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "`user`")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "lastName", nullable = false, unique = true)
    private String lastName;

    @Column(name = "role")
    private Role role;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "user")
    private List<Collection> collections;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "user")
    private List<Comment> comments;




}




