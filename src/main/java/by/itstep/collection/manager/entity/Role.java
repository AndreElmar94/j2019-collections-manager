package by.itstep.collection.manager.entity;

public enum Role {
    USER,
    ADMIN
}
